const express = require("express");
const mongoose = require("mongoose");

const VoucherRouter = require("./app/router/VoucherRouter");
const DrinkRouter = require("./app/router/DrinkRouter");
const UserRouter = require("./app/router/UserRouter");
const OrderRouter = require("./app/router/OrderRouter");

const app = express();

app.use(
  express.urlencoded({
    extended: true,
  })
);
app.use(express.json());

const port = 5000;

async function connectMongoDB() {
  await mongoose.connect("mongodb://localhost:27017/CRUD_Voucher");
}

// kết nối mongo
connectMongoDB()
  .then(() => console.log("Connect successfully!"))
  .catch((err) => console.log(err));

app.get("/", (request, response) => {
  response.json({
    message: "CRUD Voucher Rest API",
  });
});

app.use("/vouchers", VoucherRouter);
app.use("/drinks", DrinkRouter);
app.use("/users", UserRouter);
app.use("/orders", OrderRouter);

app.listen(port, () => {
  console.log(`CRUD app listening at http://localhost:${port}`);
});
