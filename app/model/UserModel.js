// Import thư viện mongoose
const mongoose = require("mongoose");

const { Schema } = mongoose;

const userSchema = new Schema({
  _id: Schema.Types.ObjectId,
  fullName: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  address: {
    type: String,
    default: true,
  },
  phone: {
    type: String,
    required: true,
  },
  noStudent: {
    type: Number,
    default: 0,
  },

  orders: [
    {
      type: Schema.Types.ObjectId,
      ref: "Order",
    },
  ],
});

const UserModel = mongoose.model("User", userSchema);

module.exports = { UserModel };
