const mongoose = require("mongoose");

const { Schema } = mongoose;

const drinkSchema = new Schema({
  _id: Schema.Types.ObjectId,
  title: {
    type: String,
    required: true,
    unique: true,
  },
  description: {
    type: String,
    required: false,
  },
  noStudent: {
    type: Number,
    default: 0,
  },
});

const DrinkModel = mongoose.model("Drink", drinkSchema);

module.exports = { DrinkModel };
