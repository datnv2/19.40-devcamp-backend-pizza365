const mongoose = require("mongoose");

const { Schema } = mongoose;

const orderSchema = new Schema({
  _id: Schema.Types.ObjectId,

  orderCode: {
    type: Schema.Types.ObjectId,
    //required: true,
  },
  pizzaSize: {
    type: String,
    required: true,
  },
  pizzaType: {
    type: String,
    required: true,
  },
  voucher: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    required: true,
  },
  createdDate: {
    type: String,
    required: true,
  },
});

const OrderModel = mongoose.model("Order", orderSchema);

module.exports = { OrderModel };
