const mongoose = require("mongoose");
const { UserModel } = require("../model/UserModel");

// hàm tạo mới user
function createUser(request, response) {
  const user = new UserModel({
    _id: mongoose.Types.ObjectId(),
    fullName: request.body.fullName,
    email: request.body.email,
    address: request.body.address,
    phone: request.body.phone,
    address: request.body.address,
    noStudent: request.body.noStudent,
  });

  user
    .save()
    .then((newUser) => {
      return response.status(200).json({
        message: "Success",
        user: newUser,
      });
    })
    .catch((error) => {
      return response.status(500).json({
        message: "Fail",
        error: error.message,
      });
    });
}

// hàm lấy ra toàn bộ drink
function getAllUser(request, response) {
  const condition = {};

  if (request.query.noStudent) {
    condition.noStudent = request.query.noStudent;
  }

  UserModel.find(condition)
    .select("_id fullName email address phone address noStudent ")
    .then((userList) => {
      return response.status(200).json({
        message: "Success",
        users: userList,
      });
    })
    .catch((error) => {
      return response.status(500).json({
        message: "Fail",
        error: error.message,
      });
    });
}

// hàm này lấy ra user dựa vào id
function getOneUser(request, response) {
  const userId = request.params.userId;
  if (mongoose.Types.ObjectId.isValid(userId)) {
    UserModel.findById(userId)
      .then((data) => {
        if (data) {
          return response.status(200).json({
            message: "Success",
            user: data,
          });
        } else {
          return response.status(404).json({
            message: "Fail",
            user: "Not found",
          });
        }
      })

      .catch((error) => {
        return response.status(500).json({
          message: "Fail",
          error: error.message,
        });
      });
  } else {
    return response.status(400).json({
      message: "Fail",
      error: "UserID is not valid",
    });
  }
}

// hàm này thực hiện update user theo id
function updateUser(request, response) {
  const userId = request.params.userId;

  const updateObject = request.body;

  if (mongoose.Types.ObjectId.isValid(userId)) {
    UserModel.findByIdAndUpdate(userId, updateObject)
      .then((updatedUser) => {
        return response.status(200).json({
          message: "success",
          updatedUser: updatedUser,
        });
      })
      .catch((error) => {
        return response.status(500).json({
          message: "Fail",
          error: error.message,
        });
      });
  } else {
    return response.status(400).json({
      message: "Fail",
      error: "UserID is not valid",
    });
  }
}

// hàm này thực hiện việc xóa user dựa vào id
function deleteUser(request, response) {
  const userId = request.params.userId;

  if (mongoose.Types.ObjectId.isValid(userId)) {
    UserModel.findByIdAndDelete(userId)
      .then(() => {
        return response.status(200).json({
          message: "Success",
        });
      })
      .catch((error) => {
        return response.status(500).json({
          message: "Fail",
          error: error.message,
        });
      });
  } else {
    return response.status(400).json({
      message: "Fail",
      error: "UserID is not valid",
    });
  }
}
module.exports = {
  createUser,
  getAllUser,
  getOneUser,
  updateUser,
  deleteUser,
};
