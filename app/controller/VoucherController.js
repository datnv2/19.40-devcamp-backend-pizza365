const mongoose = require("mongoose");
const { VoucherModel } = require("../model/VoucherModel");

// hàm tạo mới voucher
function createVoucher(request, response) {
  const voucher = new VoucherModel({
    _id: mongoose.Types.ObjectId(),
    title: request.body.title,
    description: request.body.description,
    noStudent: request.body.noStudent,
  });

  voucher
    .save()
    .then((newVoucher) => {
      return response.status(200).json({
        message: "Success",
        voucher: newVoucher,
      });
    })
    .catch((error) => {
      return response.status(500).json({
        message: "Fail",
        error: error.message,
      });
    });
}

// hàm lấy ra toàn bộ voucher
function getAllVoucher(request, response) {
  const condition = {};

  if (request.query.noStudent) {
    condition.noStudent = request.query.noStudent;
  }
  VoucherModel.find(condition)
    .select("_id title description noStudent")
    .then((voucherList) => {
      return response.status(200).json({
        message: "Success",
        vouchers: voucherList,
      });
    })
    .catch((error) => {
      return response.status(500).json({
        message: "Fail",
        error: error.message,
      });
    });
}

// hàm này lấy ra voucher dựa vào id
function getVoucherId(request, response) {
  const voucherId = request.params.voucherId;
  if (mongoose.Types.ObjectId.isValid(voucherId)) {
    VoucherModel.findById(voucherId)
      .then((data) => {
        if (data) {
          return response.status(200).json({
            message: "Success",
            voucher: data,
          });
        } else {
          return response.status(404).json({
            message: "Fail",
            voucher: "Not found",
          });
        }
      })

      .catch((error) => {
        return response.status(500).json({
          message: "Fail",
          error: error.message,
        });
      });
  } else {
    return response.status(400).json({
      message: "Fail",
      error: "VoucherID is not valid",
    });
  }
}

// hàm này thực hiện update voucher theo id
function updateVoucherById(request, response) {
  const voucherId = request.params.voucherId;

  const updateObject = request.body;

  if (mongoose.Types.ObjectId.isValid(voucherId)) {
    VoucherModel.findByIdAndUpdate(voucherId, updateObject)
      .then((updatedVoucher) => {
        return response.status(200).json({
          message: "success",
          updatedVoucher: updatedVoucher,
        });
      })
      .catch((error) => {
        return response.status(500).json({
          message: "Fail",
          error: error.message,
        });
      });
  } else {
    return response.status(400).json({
      message: "Fail",
      error: "VoucherID is not valid",
    });
  }
}

// hàm này thực hiện việc xóa voucher dựa vào id
function deleteVoucherById(request, response) {
  const voucherId = request.params.voucherId;

  if (mongoose.Types.ObjectId.isValid(voucherId)) {
    VoucherModel.findByIdAndDelete(voucherId)
      .then(() => {
        return response.status(200).json({
          message: "Success",
        });
      })
      .catch((error) => {
        return response.status(500).json({
          message: "Fail",
          error: error.message,
        });
      });
  } else {
    return response.status(400).json({
      message: "Fail",
      error: "VoucherID is not valid",
    });
  }
}

module.exports = {
  createVoucher,
  getAllVoucher,
  getVoucherId,
  updateVoucherById,
  deleteVoucherById,
};
