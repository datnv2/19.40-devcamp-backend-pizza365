const mongoose = require("mongoose");

const { UserModel } = require("../model/UserModel");

const { OrderModel } = require("../model/OrderModel");

// hàm này tạo mới order
function createOrder(req, res) {
  const order = new OrderModel({
    _id: mongoose.Types.ObjectId(),
    orderCode: mongoose.Types.ObjectId(),
    pizzaSize: req.body.pizzaSize,
    pizzaType: req.body.pizzaType,
    voucher: req.body.voucher,
    status: req.body.status,
    createdDate: req.body.createdDate,
  });

  order
    .save()

    .then(function (newOrder) {
      var userId = req.params.userId;

      return UserModel.findOneAndUpdate(
        { _id: userId },
        { $push: { orders: newOrder._id } },
        { new: true }
      );
    })
    .then((updatedUser) => {
      return res.status(200).json({
        success: true,
        message: "New Order created successfully on User",
        User: updatedUser,
      });
    })
    .catch((error) => {
      res.status(500).json({
        success: false,
        message: "Server error. Please try again.",
        error: error.message,
      });
    });
}

// Get all order of user
function getAllOrderOfUser(req, res) {
  const userId = req.params.userId;
  UserModel.findById(userId)
    .populate({ path: "orders" })
    .then((singleUser) => {
      res.status(200).json({
        success: true,
        message: `More orders on ${singleUser.fullName}`,
        Orders: singleUser,
      });
    })
    .catch((err) => {
      res.status(500).json({
        success: false,
        message: "This user does not exist",
        error: err.message,
      });
    });
}

// Get all order
function getAllOrder(req, res) {
  OrderModel.find()
    .select("_id orderCode pizzaSize pizzaType voucher status createdDate ")
    .then((allOrder) => {
      return res.status(200).json({
        success: true,
        message: "A list of all order",
        Order: allOrder,
      });
    })
    .catch((err) => {
      res.status(500).json({
        success: false,
        message: "Server error. Please try again.",
        error: err.message,
      });
    });
}

// hàm này để lấy ra order dựa vào id truyền vào
function getOneOrder(req, res) {
  const id = req.params.orderId;

  OrderModel.findById(id)
    .then((singleOrder) => {
      res.status(200).json({
        success: true,
        message: `Get data on Order`,
        Order: singleOrder,
      });
    })
    .catch((err) => {
      res.status(500).json({
        success: false,
        message: "This order does not exist",
        error: err.message,
      });
    });
}

// update order
function updateOrder(req, res) {
  const orderId = req.params.orderId;
  const updateObject = req.body;
  OrderModel.findByIdAndUpdate(orderId, updateObject)
    .then((updatedOrder) => {
      res.status(200).json({
        success: true,
        message: "Order is updated",
        updatedOrder: updatedOrder,
      });
    })
    .catch((err) => {
      res.status(500).json({
        success: false,
        message: "Server error. Please try again.",
      });
    });
}

// delete a order
function deleteOrder(req, res) {
  const id = req.params.orderId;

  OrderModel.findByIdAndRemove(id)
    .exec()
    .then(() =>
      res.status(200).json({
        success: true,
      })
    )
    .catch((err) =>
      res.status(500).json({
        success: false,
      })
    );
}
module.exports = {
  createOrder,
  getAllOrder,
  getOneOrder,
  updateOrder,
  deleteOrder,
  getAllOrderOfUser,
};
