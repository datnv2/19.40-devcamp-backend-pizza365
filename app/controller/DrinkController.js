const mongoose = require("mongoose");
const { DrinkModel } = require("../model/DrinkModel");

// hàm tạo mới drink
function createDrink(request, response) {
  const drink = new DrinkModel({
    _id: mongoose.Types.ObjectId(),
    title: request.body.title,
    description: request.body.description,
    noStudent: request.body.noStudent,
  });

  drink
    .save()
    .then((newDrink) => {
      return response.status(200).json({
        message: "Success",
        drink: newDrink,
      });
    })
    .catch((error) => {
      return response.status(500).json({
        message: "Fail",
        error: error.message,
      });
    });
}

// hàm lấy ra toàn bộ drink
function getAllDrink(request, response) {
  const condition = {};

  if (request.query.noStudent) {
    condition.noStudent = request.query.noStudent;
  }

  DrinkModel.find(condition)
    .select("_id title description noStudent")
    .then((drinkList) => {
      return response.status(200).json({
        message: "Success",
        drinks: drinkList,
      });
    })
    .catch((error) => {
      return response.status(500).json({
        message: "Fail",
        error: error.message,
      });
    });
}

// hàm này lấy ra drink dựa vào id
function getDrinkById(request, response) {
  const drinkId = request.params.drinkId;
  if (mongoose.Types.ObjectId.isValid(drinkId)) {
    DrinkModel.findById(drinkId)
      .then((data) => {
        if (data) {
          return response.status(200).json({
            message: "Success",
            drink: data,
          });
        } else {
          return response.status(404).json({
            message: "Fail",
            drink: "Not found",
          });
        }
      })

      .catch((error) => {
        return response.status(500).json({
          message: "Fail",
          error: error.message,
        });
      });
  } else {
    return response.status(400).json({
      message: "Fail",
      error: "DrinkID is not valid",
    });
  }
}

// hàm này thực hiện update drink theo id
function updateDrinkById(request, response) {
  const drinkId = request.params.drinkId;

  const updateObject = request.body;

  if (mongoose.Types.ObjectId.isValid(drinkId)) {
    DrinkModel.findByIdAndUpdate(drinkId, updateObject)
      .then((updatedDrink) => {
        return response.status(200).json({
          message: "success",
          updatedDrink: updatedDrink,
        });
      })
      .catch((error) => {
        return response.status(500).json({
          message: "Fail",
          error: error.message,
        });
      });
  } else {
    return response.status(400).json({
      message: "Fail",
      error: "DrinkID is not valid",
    });
  }
}

// hàm này thực hiện việc xóa drink dựa vào id
function deleteDrinkById(request, response) {
  const drinkId = request.params.drinkId;

  if (mongoose.Types.ObjectId.isValid(drinkId)) {
    DrinkModel.findByIdAndDelete(drinkId)
      .then(() => {
        return response.status(200).json({
          message: "Success",
        });
      })
      .catch((error) => {
        return response.status(500).json({
          message: "Fail",
          error: error.message,
        });
      });
  } else {
    return response.status(400).json({
      message: "Fail",
      error: "DrinkID is not valid",
    });
  }
}

module.exports = {
  createDrink,
  getAllDrink,
  getDrinkById,
  updateDrinkById,
  deleteDrinkById,
};
