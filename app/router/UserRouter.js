const express = require("express");

const {
  createUser,
  getAllUser,
  getOneUser,
  updateUser,
  deleteUser,
} = require("../controller/UserController");

const {
  createOrder,
  getAllOrderOfUser,
} = require("../controller/OrderController");

const router = express.Router();

router.post("/", createUser);
router.get("/", getAllUser);
router.get("/:userId", getOneUser);
router.put("/:userId", updateUser);
router.delete("/:userId", deleteUser);
router.post("/:userId/orders", createOrder);
router.get("/:userId/orders", getAllOrderOfUser);
module.exports = router;
