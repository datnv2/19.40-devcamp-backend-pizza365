const express = require("express");

const {
  createDrink,
  getAllDrink,
  getDrinkById,
  updateDrinkById,
  deleteDrinkById,
} = require("../controller/DrinkController");

const router = express.Router();

router.post("/", createDrink);
router.get("/", getAllDrink);
router.get("/:drinkId", getDrinkById);
router.put("/:drinkId", updateDrinkById);
router.delete("/:drinkId", deleteDrinkById);

module.exports = router;
