const express = require("express");

const {
  createVoucher,
  getAllVoucher,
  getVoucherId,
  updateVoucherById,
  deleteVoucherById,
} = require("../controller/VoucherController");

const router = express.Router();

router.post("/", createVoucher);
router.get("/", getAllVoucher);
router.get("/:voucherId", getVoucherId);
router.put("/:voucherId", updateVoucherById);
router.delete("/:voucherId", deleteVoucherById);

module.exports = router;
