const express = require("express");
const {
  getAllOrder,
  getOneOrder,
  updateOrder,
  deleteOrder,
} = require("../controller/OrderController");

const router = express.Router();

router.get("/", getAllOrder);
router.get("/:orderId", getOneOrder);
router.put("/orders/:orderId", updateOrder);
router.delete("/orders/:orderId", deleteOrder);
module.exports = router;
